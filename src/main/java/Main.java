import Controller.Controller;
import Model.Polinom;
import View.View;

public class Main {

    public static void main(String[] args) {
        Polinom polinom = new Polinom();
        View view = new View(polinom);
        Controller controller = new Controller(polinom, view);
    }

}
