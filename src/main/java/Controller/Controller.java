package Controller;

import Model.Monom;
import Model.Polinom;
import View.View;

import java.awt.event.*;

public class Controller {
    private Polinom model;
    private View view;

    public Controller(Polinom polinom, View view) {
        this.model = polinom;
        this.view  = view;

        view.addB1Listener(new AddB1Listener());
        view.addB2Listener(new AddB2Listener());
        view.addB3Listener(new AddB3Listener());
        view.addB4Listener(new AddB4Listener());
        view.addB5Listener(new AddB5Listener());
        view.addB6Listener(new AddB6Listener());
    }


    class AddB1Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            String in2;
            in1 = view.getFirst();
            in2 = view.getSecond();
            model.citire(in1,true);
            model.citire(in2,false);
            model.adunare();
            view.afisare(2,1);
            model.resetare();
        }
    }
    class AddB2Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            String in2;
            in1 = view.getFirst();
            in2 = view.getSecond();
            model.citire(in1,true);
            model.citire(in2,false);
            model.negare();
            model.adunare();
            view.afisare(2,1);
            model.resetare();
        }
    }
    class AddB3Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            String in2;
            in1 = view.getFirst();
            in2 = view.getSecond();
            model.citire(in1,true);
            model.citire(in2,false);
            model.inmultire();
            view.afisare(3,1);
            model.resetare();
        }
    }
    class AddB4Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            String in2;
            in1 = view.getFirst();
            in2 = view.getSecond();
            model.citire(in1,true);
            model.citire(in2,false);
            model.impartire();
            view.afisare(3,2);
            model.resetare();
        }
    }
    class AddB5Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            in1 = view.getFirst();
            model.citire(in1,true);
            model.derivare();
            view.afisare(1,1);
            model.resetare();
        }
    }
    class AddB6Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            in1 = view.getFirst();
            model.citire(in1,true);
            model.integrare();
            view.afisare(1,3);
            model.resetare();
        }
    }
}
