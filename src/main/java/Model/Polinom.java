package Model;

import Model.Monom;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
    ArrayList<Monom> first;
    ArrayList<Monom> second;
    ArrayList<Monom> third;
    ArrayList<Monom> result;

    public Polinom()
    {
        first = new ArrayList<Monom>();
        second = new ArrayList<Monom>();
        third = new ArrayList<Monom>();
        result = new ArrayList<Monom>();
    }

    private void addToList(boolean n, int a, int b) {
        if(n)
            first.add(new Monom(a,b));
        else
            second.add(new Monom(a,b));
    }

    private void addXToList(boolean n, boolean x, int a) {
        if(a!=1 || x){
            addToList(n,a,1);
            if(n)
                Collections.sort(first);
            else
                Collections.sort(second);
        }
    }

    public void citire(String s, boolean n) {
        int a = 1, b = 0, c = 0;
        boolean x = false;
        String patternStr = "[-^]*\\d+[* ]*";
        Pattern pattern= Pattern.compile(patternStr);
        Matcher matcher= pattern.matcher(s);
        while(matcher.find()) {
            String k = matcher.group();
            if (k.indexOf('*') != -1) {
                k = k.substring(0,k.length()-1);
                a = Integer.parseInt(k);
                x = true;
            }
            else if (k.indexOf('^') != -1) {
                k = k.substring(1,k.length());
                b = Integer.parseInt(k);
                addToList(n,a,b);
                a = 1;
                x = false;
            }
            else {
                c = Integer.parseInt(k);
                addToList(n,c,0);
            }
        }
        addXToList(n,x,a);

    }

    public void adunare() {
        boolean k = true;
        for (Monom i : first) {
            for(Monom j : second) {
                if(i.getExp() == j.getExp()) {
                    j.addCoef(i.getCoef());
                    k = false;
                }
            }
            if(k) {
                second.add(i);
            }
            k = true;
        }
        Collections.sort(second);
    }

    public void negare() {
        for (Monom j : second) {
            j.setNegCoef();
        }
    }

    public void inmultire() {
        int a = 0, b = 0;
        for (Monom i : first) {
            for(Monom j : second) {
                a = i.getCoef() * j.getCoef();
                b = i.getExp() + j.getExp();
                third.add(new Monom(a,b));
            }
        }
        Collections.sort(third);
        for(Monom i : third) {
            if(result.isEmpty()) {
                result.add(i);
            }
            else if(result.get(result.size()-1).getExp() == i.getExp()) {
                result.get(result.size()-1).addCoef(i.getCoef());
            }
            else {
                result.add(i);
            }
        }
    }

    public void impartire() {
        int a = 0;
        double b = 0;
        if(first.isEmpty() || second.isEmpty())
            return;
        for(Monom j : first) {
            a = j.getExp() - second.get(0).getExp();
            b = j.getCoefDouble() / second.get(0).getCoefDouble();
            result.add(new Monom(b,a));
            if (a >= 0) {
                for(Monom i : second) {
                    third.add(new Monom(i.getCoefDouble()*b,i.getExp()+a));
                }
            }
            else
                return;
            sub();
            third = new ArrayList<Monom>();
        }
    }

    private void sub() {
        for (Monom j : third) {
            j.setNegCoef();
        }
        for (Monom i : third) {
            for(Monom j : first) {
                if(i.getExp() == j.getExp()) {
                    j.addCoef(i.getCoefDouble());
                }
            }
        }
    }

    public void derivare() {
        for (Monom i : first) {
            i.setCoef(i.getCoef()*i.getExp());
            i.setExp(i.getExp()-1);
        }
    }

    public void integrare() {
        for (Monom i : first) {
            i.setExp(i.getExp()+1);
            i.setCoef(i.getCoefDouble()/i.getExp());
        }
    }

    public void resetare() {
        first = new ArrayList<Monom>();
        second = new ArrayList<Monom>();
        third = new ArrayList<Monom>();
        result = new ArrayList<Monom>();
    }
    public void clearFirst() {
        first = new ArrayList<Monom>();
    }

    public void clearSecond() {
        second = new ArrayList<Monom>();
    }

    public ArrayList<Monom> getList(int i)
    {
        if(i == 1)
            return first;
        else if(i == 2)
            return second;
        else
            return result;
    }

}
