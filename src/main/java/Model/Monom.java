package Model;

public class Monom implements Comparable<Object>{
    private double coef;
    private int exp;

    public Monom (int coef, int exp)
    {
        this.coef = coef;
        this.exp = exp;
    }

    public Monom (double coef, int exp)
    {
        this.coef = coef;
        this.exp = exp;
    }

    public void addCoef(int coef) {
        this.coef = this.coef + coef;
    }

    public void addCoef(double coef) {
        this.coef = this.coef + coef;
    }

    public void setNegCoef() {
        this.coef = -1 * (this.coef);
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getCoef() {
        return (int)coef;
    }

    public double getCoefDouble() {
        return coef;
    }

    public int getExp() {
        return exp;
    }

    @Override
    public int compareTo(Object comp) {
        int compareExp=((Monom)comp).getExp();
        return compareExp-this.exp;
    }
}
