package View;

import Controller.Controller;
import Model.Monom;
import Model.Polinom;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class View extends JFrame{

    public JLabel l0 = new JLabel ("Polynomial Calculator");
    public JLabel l1 = new JLabel ("First polynomial: ");
    public JLabel l2 = new JLabel ("Second polynomial: ");
    public JLabel l3 = new JLabel ("Result: ");
    public JTextArea tf1 = new JTextArea(1,10);
    public JTextArea tf2 = new JTextArea(1,10);
    public JTextArea tf3 = new JTextArea(1,10);
    public JButton b1 = new JButton("Addition");
    public JButton b2 = new JButton("Subtraction");
    public JButton b3 = new JButton("Multiplication");
    public JButton b4 = new JButton("Division");
    public JButton b5 = new JButton("Derivative");
    public JButton b6 = new JButton("Integral");
    private Polinom model;

    public View (Polinom model) {
        this.model = model;

        JFrame frame = new JFrame ("Polynomial Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(250, 300);

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel panel6 = new JPanel();
        JPanel panel7 = new JPanel();
        JPanel panel8 = new JPanel();
        JPanel panel9 = new JPanel();

        GridLayout grLayout = new GridLayout(0,1,0,1);
        FlowLayout flLayout = new FlowLayout();

        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(grLayout);
        panel3.setLayout(grLayout);
        panel4.setLayout(grLayout);
        panel5.setLayout(grLayout);
        panel6.setLayout(flLayout);
        panel7.setLayout(flLayout);
        panel8.setLayout(flLayout);
        panel9.setLayout(flLayout);

        panel8.add(l0);
        panel1.add(panel8);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel2.add(l1);
        panel2.add(l2);
        panel3.add(tf1);
        panel3.add(tf2);

        panel6.add(panel2);
        panel6.add(panel3);
        panel1.add(panel6);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel9.add(l3);
        panel9.add(tf3);
        panel1.add(panel9);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel4.add(b1);
        panel4.add(b3);
        panel4.add(b5);
        panel5.add(b2);
        panel5.add(b4);
        panel5.add(b6);

        panel7.add(panel4);
        panel7.add(panel5);
        panel1.add(panel7);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );


        frame.setContentPane(panel1);
        frame.setVisible(true);
    }

    public String getFirst() {
        return tf1.getText();
    }
    public String getSecond() {
        return tf2.getText();
    }



    public void afisare(int j, int x){
        ArrayList<Monom> lista = model.getList(j);
        String s= "";
        if(lista.isEmpty()) {
            if(x == 2)
                tf3.setText("0/-");
            else if(x == 3)
                tf3.setText("c");
            else
                tf3.setText("0");
            return;
        }
        boolean k = false;
        for(Monom i : lista)
            if(i.getCoefDouble() != 0 && i.getExp() >= 0) {
                if(i.getCoefDouble() > 0 && k)
                    s+="+";
                if(x == 3 || x == 2)
                    s = s + BigDecimal.valueOf(i.getCoefDouble()).setScale(2, RoundingMode.DOWN);
                else
                    s = s + i.getCoef();
                s = s + "*x^" + i.getExp();
                k = true;
            }
        if(x == 2)
            s = "q = " + s + "\nr = " + afisareRest();
        else if(x == 3)
            s = s + "+c";
        tf3.setText(s);
    }

    private String afisareRest() {
        ArrayList<Monom> rest = model.getList(1);
        String s= "";
        boolean k = false;
        for(Monom i : rest)
            if(i.getCoef() != 0 && i.getExp() >= 0) {
                if(i.getCoef() > 0 && k) {
                    s+="+";
                }
                s = s + BigDecimal.valueOf(i.getCoefDouble()).setScale(2, RoundingMode.HALF_DOWN) + "*x^" + i.getExp();
                k = true;
            }
        return s;
    }

    public void addB1Listener(ActionListener add) {
        b1.addActionListener(add);
    }

    public void addB2Listener(ActionListener sub) {
        b2.addActionListener(sub);
    }

    public void addB3Listener(ActionListener mul) {
        b3.addActionListener(mul);
    }

    public void addB4Listener(ActionListener div) {
        b4.addActionListener(div);
    }

    public void addB5Listener(ActionListener der) {
        b5.addActionListener(der);
    }

    public void addB6Listener(ActionListener integ) {
        b6.addActionListener(integ);
    }
}
