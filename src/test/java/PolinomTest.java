import Model.Monom;
import Model.Polinom;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PolinomTest {

    private static Polinom m;
    private static int nrTesteExecutate = 0;
    private static int nrTesteSucces = 0;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        m = new Polinom();
        nrTesteExecutate++;
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        System.out.println("S-a executat un test cu succes!");
        System.out.println("Teste executate: " + nrTesteExecutate + " Teste succes: " + nrTesteSucces);
    }

    @org.junit.jupiter.api.Test
    void adunare() {
        m.citire("2*x^3+3*x^2+3*x+1",true);
        m.citire("x^1+1",false);
        m.adunare();
        ArrayList<Monom> list = m.getList(2);
        assertEquals(list.get(0).getCoef(),2);
        assertEquals(list.get(0).getExp(),3);
        assertEquals(list.get(1).getCoef(),3);
        assertEquals(list.get(1).getExp(),2);
        assertEquals(list.get(2).getCoef(),4);
        assertEquals(list.get(2).getExp(),1);
        assertEquals(list.get(3).getCoef(),2);
        assertEquals(list.get(3).getExp(),0);
        nrTesteSucces++;
    }

    @org.junit.jupiter.api.Test
    void negare() {
        m.citire("2*x^2+1*x+3",false);
        m.negare();
        ArrayList<Monom> list = m.getList(2);
        assertEquals(list.get(0).getCoef(),-2);
        assertEquals(list.get(0).getExp(),2);
        assertEquals(list.get(1).getCoef(),-1);
        assertEquals(list.get(1).getExp(),1);
        assertEquals(list.get(2).getCoef(),-3);
        assertEquals(list.get(2).getExp(),0);
        nrTesteSucces++;
    }

    @org.junit.jupiter.api.Test
    void inmultire() {
        m.citire("2*x^2-1*x+3",true);
        m.citire("x^1+1",false);
        m.inmultire();
        m.citire("2*x^3+x^2+2*x+3",true);
        ArrayList<Monom> list = m.getList(3);
        assertEquals(list.get(0).getCoef(),2);
        assertEquals(list.get(0).getExp(),3);
        assertEquals(list.get(1).getCoef(),1);
        assertEquals(list.get(1).getExp(),2);
        assertEquals(list.get(2).getCoef(),2);
        assertEquals(list.get(2).getExp(),1);
        assertEquals(list.get(3).getCoef(),3);
        assertEquals(list.get(3).getExp(),0);
        nrTesteSucces++;
    }

    @org.junit.jupiter.api.Test
    void impartire() {
        m.citire("x^3+2*x^2-1*x+3",true);
        m.citire("1*x+1",false);
        m.impartire();
        ArrayList<Monom> list = m.getList(3);
        assertEquals(list.get(0).getCoef(),1);
        assertEquals(list.get(0).getExp(),2);
        assertEquals(list.get(1).getCoef(),1);
        assertEquals(list.get(1).getExp(),1);
        assertEquals(list.get(2).getCoef(),-2);
        assertEquals(list.get(2).getExp(),0);
        list = m.getList(1);
        assertEquals(list.get(0).getCoef(),0);
        assertEquals(list.get(0).getExp(),3);
        assertEquals(list.get(1).getCoef(),0);
        assertEquals(list.get(1).getExp(),2);
        assertEquals(list.get(2).getCoef(),0);
        assertEquals(list.get(2).getExp(),1);
        assertEquals(list.get(3).getCoef(),5);
        assertEquals(list.get(3).getExp(),0);
        nrTesteSucces++;
    }

    @org.junit.jupiter.api.Test
    void derivare() {
        m.citire("x^3+2*x^2-1*x+3",true);
        m.citire("3*x^2+4*x-1",false);
        m.derivare();
        ArrayList<Monom> list = m.getList(1);
        assertEquals(list.get(0).getCoef(),3);
        assertEquals(list.get(0).getExp(),2);
        assertEquals(list.get(1).getCoef(),4);
        assertEquals(list.get(1).getExp(),1);
        assertEquals(list.get(2).getCoef(),-1);
        assertEquals(list.get(2).getExp(),0);
        assertEquals(list.get(3).getCoef(),0);
        assertEquals(list.get(3).getExp(),-1);
        nrTesteSucces++;
    }

    @org.junit.jupiter.api.Test
    void integrare() {
        m.citire("4*x^3+3*x^2-2*x+3",true);
        m.integrare();
        ArrayList<Monom> list = m.getList(1);
        assertEquals(list.get(0).getCoef(),1);
        assertEquals(list.get(0).getExp(),4);
        assertEquals(list.get(1).getCoef(),1);
        assertEquals(list.get(1).getExp(),3);
        assertEquals(list.get(2).getCoef(),-1);
        assertEquals(list.get(2).getExp(),2);
        assertEquals(list.get(3).getCoef(),3);
        assertEquals(list.get(3).getExp(),1);
        nrTesteSucces++;
    }
}